import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_mixer.json"                                                               #Pfad zur json Datei des 2 Versuches (muss je nach Versuch angepasst werden)
measure_duration_in_s = 20                                                                                    #Messdauer des Versuches 

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
dict_data = {}                                                                                                  #Leeres Dictionary für das ablegen der Messdaten erstellen
sensor_UUIDs = {
    "1ee847be-fddd-6ee4-892a-68c4555b0981": "1ee847be-fddd-6ee4-892a-68c4555b0981"                              #UUID des Beschleunigungssensors
}
# das innere Dictionary mit leeren Listen 
dict_data[sensor_UUIDs["1ee847be-fddd-6ee4-892a-68c4555b0981"]] = {                                              #Leere Listen erstellen in denen die Messdaten dann abgelegt werden
    "acceleration_x": [],
    "acceleration_y": [],
    "acceleration_z": [],
    "timestamp": []
}

# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
i2c = board.I2C()
accelerometer = adafruit_adxl34x.ADXL345(i2c)

i = 0                                                                                                                    
while i <= measure_duration_in_s:                                                                                    #While Schleife läuft solange i kleiner als die Messzeit in Sekunden ist
    acceleration_values = accelerometer.acceleration
    dict_data[sensor_UUIDs["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["acceleration_x"].append(acceleration_values[0]) #Die Messdaten werden in den Listen die im Dictionary sind abgelegt
    dict_data[sensor_UUIDs["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["acceleration_y"].append(acceleration_values[1])
    dict_data[sensor_UUIDs["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["acceleration_z"].append(acceleration_values[2])
    dict_data[sensor_UUIDs["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["timestamp"].append(i)
    i += 0.001                                                                                                       #Index zählt um 0.001 nach oben bei jedem Durchgang
    time.sleep(0.001)                                                                                                #Delay time nach jeder Messung
# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
name = path_measurement_folder[17:]
print(name)
with h5py.File(path_h5_file, "w") as h5_file:                                                                        #Öffne die vorher erstellte leere HDF5 Datei als 'w'
    raw_data = h5_file.create_group("RawData")                                                                       #Lege eine Subgroup "RawData" an 
    sensor_group = raw_data.create_group(sensor_UUIDs["1ee847be-fddd-6ee4-892a-68c4555b0981"])                       #Lege eine Subgroup "1ee847be-fddd-6ee4-892a-68c4555b0981" in "RawData" an
    sensor_group.create_dataset("acceleration_x", data=dict_data[sensor_UUIDs["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["acceleration_x"]) #in der Subgroup "1ee847be-fddd-6ee4-892a-68c4555b0981" werden nun die Datasets mit den Messwerten und den Zeiten abgelegt
    sensor_group.create_dataset("acceleration_y", data=dict_data[sensor_UUIDs["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["acceleration_y"])
    sensor_group.create_dataset("acceleration_z", data=dict_data[sensor_UUIDs["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["acceleration_z"])
    sensor_group.create_dataset("timestamp", data=dict_data[sensor_UUIDs["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["timestamp"])
   
    sensor_group["acceleration_x"].attrs["unit"] = "m/s^2"                                                           #Die Einheiten werden an den richtigen Datasets hinzugefügt
    sensor_group["acceleration_y"].attrs["unit"] = "m/s^2"
    sensor_group["acceleration_z"].attrs["unit"] = "m/s^2"
    sensor_group["timestamp"].attrs["unit"] = "s"

print(f"Die Messungen wurden abgeschlossen. Die Daten wurden in der HDF5-Datei '{path_h5_file}' abgespeichert.")

# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
